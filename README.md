# Community translation of SOB

## Most is machine translated, however all of the buttons in the game are currently all translated. The main body text will take awhile to fully complete.

## Character name changes
* Glad/Happy --- Rada
* Eugene ------- Eugenia
* Light -------- Sveta
* Kate --------- Katya
* Aurelius ----- Aurelia
* Baena -------- Bazhena
* Jan/Ian ------ Yana
* Considering changing the names of the Twins in body text since two other characters have the same name as them. Dasha and Masha but unsure as to what right now.

## Some important changes
Katya's father's questions have been fixed. Choices are now A, B, C or D and questions are clear.

## Scripts
These are more for devs to split the main qsp into separate files, easier to work on with a more advanced editor. Plus merge the split files back into one large file.


